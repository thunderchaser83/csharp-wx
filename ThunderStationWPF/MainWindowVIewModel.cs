﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Meteorology;
using System.Windows.Input;
using GeoJsonWeather;
using System.ComponentModel;

namespace ThunderStationWPF
{
    public class MainWindowViewModel : ViewModelBase
    {
        private List<ASOSStation> stationList;

        WeatherObservation Observation { get; set; }
        List<ASOSStation> StationList
        {
            get { return ASOSStation.GetStations(); }
            set
            {
                this.stationList = value;
                OnPropertyChanged(nameof(StationList));
            }
        }
        RelayCommand _getObsCommand; public ICommand GetObsCommand
        {
            get
            {
                if (_getObsCommand == null)
                {
                    _getObsCommand = new RelayCommand(param => Observation.GetObs());
                }
                return _getObsCommand;
            }
        }

        public MainWindowViewModel()
        {
            this.Observation = new WeatherObservation();
        }
    }
}
