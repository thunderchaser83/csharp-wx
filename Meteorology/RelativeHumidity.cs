﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meteorology
{
    public class RelativeHumidity
    {
        private double value;

        public double Value { get; set; }

        public RelativeHumidity(double v)
        {
            this.Value = v;
        }

        public static RelativeHumidity Calculate(VaporPressure es, VaporPressure e)
        {
            return e / es * 100;
        }

        public static RelativeHumidity operator +(RelativeHumidity f, double v)
        {
            return new RelativeHumidity(f.Value + v);
        }

        public static RelativeHumidity operator *(RelativeHumidity f, double v)
        {
            return new RelativeHumidity(f.Value * v);
        }

        public static RelativeHumidity operator -(RelativeHumidity f, double v)
        {
            return new RelativeHumidity(f.Value - v);
        }

        public static implicit operator double(RelativeHumidity t)
        {
            return t.Value;
        }

        public static implicit operator RelativeHumidity(double t)
        {
            return new RelativeHumidity(t);
        }
    }
}
