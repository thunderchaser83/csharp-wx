﻿using System;
using System.Collections.Generic;
using csharp_metar_decoder;
using System.IO;
using System.Diagnostics;
using GeoJsonWeather;
using Newtonsoft.Json.Linq;
using System.ComponentModel;

namespace Meteorology
{
    
    public class WeatherObservation : FeatureCollection
    {
        public Fahrenheit Temperature { get; set; }
        public HeatIndex HeatIndex { get; set; }
        public DewPoint DewPoint { get; set; }
        public RelativeHumidity RH { get; set; }
        public Wind Wind { get; set; }
        public WindChill WindChill { get; set; }
        public Pressure Pressure { get; set; }
        public DateTime Timestamp { get; set; }
        public ASOSStation SelectedStation { get; set; }


        public WeatherObservation()
        {

        }
        public WeatherObservation(Fahrenheit t, Pressure p, Wind w, RelativeHumidity rh)
        {
            this.Temperature = t;
            this.Pressure = p;
            this.Wind = w;
            this.RH = rh;
            this.HeatIndex = HeatIndex.Calculate(t, this.RH);
            this.DewPoint = DewPoint.Calculate(this.Temperature.ToCelsius(), this.RH, this.Pressure);
            this.WindChill = WindChill.Calculate(this.Temperature, this.Wind.Speed);
        }

        public static List<WeatherObservation> ReadMetarFromCSV()
        {
            List<WeatherObservation> obsList = new List<WeatherObservation>();

            string[] rawMetar = File.ReadAllLines(@"D:\Downloads\K5C1Metar.txt");

            foreach (string m in rawMetar)
            {
                DateTime ts;
                try
                {
                    string metar = m.Split(',')[2];
                    if (m.Length > 30)
                    {
                        var d = MetarDecoder.ParseWithMode(metar);
                        WeatherObservation ws = new WeatherObservation(
                            new Celsius(d.AirTemperature.ActualValue).ToFahrenheit(),
                            new Pressure(d.Pressure.ActualValue),
                            new Wind()
                            {
                                Speed = d.SurfaceWind.MeanSpeed.ActualValue,
                                Direction = new Direction(d.SurfaceWind.MeanDirection.ActualValue)
                            },
                            new RelativeHumidity(52));
                        ws.Timestamp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, Convert.ToInt32(d.Day.Value), Convert.ToInt32(d.Time.Replace("UTC", "").Substring(0, 2)), Convert.ToInt32(d.Time.Replace("UTC", "").Substring(3, 2)), 0);
                        ws.Timestamp = ws.Timestamp.ToLocalTime();
                        obsList.Add(ws);

                        
                    }
                } catch (Exception ex)
                {
                    Debug.Print(ex.Message);
                }
            }
            return obsList;
        }

        public static WeatherObservation FromJToken(JToken o)
        {
            WeatherObservation wo = new WeatherObservation();
            Celsius cTemp = Convert.ToDouble(o["properties"]["temperature"]["value"].ToString());
            wo.Temperature = cTemp.ToFahrenheit();
            Celsius dTemp = (Celsius)Convert.ToDouble(o["properties"]["dewpoint"]["value"].ToString());
            wo.DewPoint = new DewPoint(dTemp.ToFahrenheit());
            wo.Wind = new Wind();
            wo.Wind.Speed = o["properties"]["windSpeed"]["value"].ToString() == "" ? 0 : (Celsius)Convert.ToDouble(o["properties"]["windSpeed"]["value"].ToString()) * 2.237;
            wo.Wind.Direction = o["properties"]["windDirection"]["value"].ToString() == "" ? null : new Direction((Celsius)Convert.ToDouble(o["properties"]["windDirection"]["value"].ToString()));
            wo.RH = new RelativeHumidity((Celsius)Convert.ToDouble(o["properties"]["relativeHumidity"]["value"].ToString()));
            wo.Pressure = new Pressure((Celsius)Convert.ToDouble(o["properties"]["barometricPressure"]["value"].ToString()) / 100);
            wo.Timestamp = DateTime.Parse(o["properties"]["timestamp"].ToString());
            wo.WindChill = WindChill.Calculate(wo.Temperature, wo.Wind.Speed);
            return wo;
        }

        public void GetObs()
        {
            JToken o = FetchData(String.Format("https://api.weather.gov/stations/{0}/observations/current", this.SelectedStation), FeatureCollection.GetFeature);
            Celsius cTemp = Convert.ToDouble(o["properties"]["temperature"]["value"].ToString());
            this.Temperature = cTemp.ToFahrenheit();
            Celsius dTemp = Convert.ToDouble(o["properties"]["dewpoint"]["value"].ToString());
            this.DewPoint = new DewPoint(dTemp.ToFahrenheit());
            this.Wind = new Wind();
            this.Wind.Speed = (Celsius)Convert.ToDouble(o["properties"]["windSpeed"]["value"].ToString()) * 2.237;
            this.Wind.Direction = new Direction((Celsius)Convert.ToDouble(o["properties"]["windDirection"]["value"].ToString()));
            this.RH = new RelativeHumidity((Celsius)Convert.ToDouble(o["properties"]["relativeHumidity"]["value"].ToString()));
            this.Pressure = new Pressure((Celsius)Convert.ToDouble(o["properties"]["barometricPressure"]["value"].ToString()) / 100);
            this.Timestamp = DateTime.Parse(o["properties"]["timestamp"].ToString());
            this.WindChill = WindChill.Calculate(this.Temperature, this.Wind.Speed);
        }
    }
}
