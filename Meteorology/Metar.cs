﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meteorology
{
    public class Metar
    {
        public string ICAOStation { get; private set; }
        public DateTime Timestamp { get; private set; }

        public Metar Parse(string m)
        {
            string metar = m.Split(',')[1];
            return new Metar();
        }
    }
}
