﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meteorology
{
    public interface ITemperature
    {
        double Value { get; set; }
    }
}
