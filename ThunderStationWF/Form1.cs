﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Meteorology;
using System.Threading;
using System.Windows.Forms.DataVisualization.Charting;
using GeoJsonWeather;
using System.Diagnostics;
using Newtonsoft.Json.Linq;

namespace ThunderStationWF
{
    public partial class Form1 : Form
    {
        Point? previousPosition = null;
        ToolTip toolTip = new ToolTip();
        WeatherObservation obs;
        List<ASOSStation> stationList;
        System.Timers.Timer timer = new System.Timers.Timer();
        System.Timers.Timer clockTimer = new System.Timers.Timer();
        System.Timers.Timer alertTimer = new System.Timers.Timer();
        TimeSpan ts = TimeSpan.FromHours(1);
        FeatureCollection collection = new FeatureCollection();
        BindingList<Alert> filteredAlerts = new BindingList<Alert>();
        IAlert alrt;
        WarningForm wf;        

        public Form1()
        {
            InitializeComponent();
            wf = new WarningForm();

            this.Load += Form1_Load;
            this.alertsDataGrid.CellClick += AlertsDataGrid_CellClick;
            this.collection.OnAlertIssued += Collection_OnAlertIssued1;

            collection.OnAlertIssued += Collection_OnAlertIssued;
            collection.OnAlertMessage += Collection_OnAlertMessage;

            stationList = ASOSStation.GetStations();
            stationList = stationList.OrderBy(s => s.Name).ToList();

            this.stationListComboBox.DataSource = this.stationList;
            this.stationListComboBox.DisplayMember = "Name";
            this.stationListComboBox.ValueMember = "Identifier";

            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 1000 * 60 * 60;
            timer.Enabled = true;

            clockTimer.Elapsed += ClockTimer_Tick;
            clockTimer.Interval = 1000;
            clockTimer.Enabled = true;

            this.alertTimer.Elapsed += AlertTimer_Elapsed;
            this.alertTimer.Interval = 1000 * 60 * 3;
            this.alertTimer.Start();

        }

        private void SelectedAlertsCheckListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            AlertType alt = (AlertType)this.SelectedAlertsCheckListBox.Items[e.Index];
            if (e.NewValue == CheckState.Checked)
            {
                if (!this.collection.AlertsToShow.Contains(alt))
                {
                    this.collection.AlertsToShow.Add(alt);
                }
            } else if (e.NewValue == CheckState.Unchecked)
            {
                if (this.collection.AlertsToShow.Contains(alt))
                {
                    this.collection.AlertsToShow.Remove(alt);
                }
            }
        }

        private async void AlertTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(async () =>
                {
                    await this.GetAlerts();
                }));
            } else
            {
                await this.GetAlerts();
            }
        }

        private void Collection_OnAlertIssued1(object sender, AlertIssuedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(() =>
                {
                    this.alrt = e.Alert;
                    wf.ChangeAlert(e.Alert);
                    if (!wf.Visible)
                    {
                        wf.Show();
                    }
                }));
            }

            System.Media.SoundPlayer player = new System.Media.SoundPlayer(Properties.Resources.eas);
            player.Play();
        }

        private void AlertsDataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            IAlert alert = (IAlert)this.alertsDataGrid.Rows[e.RowIndex].DataBoundItem;
            this.expiresTextBox.Text = alert.Expires.ToString();
            this.senderTextBox.Text = alert.SenderName;
            this.headlineTextBox.Text = alert.Headline;
            this.descriptionTextBox.Text = alert.Description;
            this.groupBox1.Text = alert.Event;
            this.tabPage2.BackColor = alert.PrimaryColor;
        }

        private async Task GetAlerts()
        {
            this.toolStripStatusLabel1.Text = "Downloading data...";
            await collection.Start();

            this.alertsDataGrid.DataSource = collection.FilteredAlerts;
            this.toolStripStatusLabel1.Text = "Ready";
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            
            await GetAlerts();
            
            this.SelectedAlertsCheckListBox.DataSource = Enum.GetValues(typeof(AlertType));
            this.SelectedAlertsCheckListBox.ItemCheck += SelectedAlertsCheckListBox_ItemCheck;

            for (int i = 0; i < this.SelectedAlertsCheckListBox.Items.Count; i++)
            {
                if (this.collection.AlertsToShow.Contains((AlertType)this.SelectedAlertsCheckListBox.Items[i]))
                {
                    this.SelectedAlertsCheckListBox.SetItemChecked(i, true);
                }
                else
                {
                    this.SelectedAlertsCheckListBox.SetItemChecked(i, false);
                }
            }


            this.chart1.Visible = false;
            this.chart1.Series["Temperature"].Color = Color.DarkRed;
            this.chart1.Series["Dew Point"].Color = Color.DarkGreen;
            this.chart1.Series["Pressure"].Color = Color.DarkBlue;
            this.chart1.Series["Temperature"].XValueType = ChartValueType.DateTime;
            this.chart1.Series["Dew Point"].XValueType = ChartValueType.DateTime;
            this.chart1.Series["Pressure"].XValueType = ChartValueType.DateTime;

            foreach (Control c in this.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    c.Enabled = false;
                }
                if (c.GetType() == typeof(CheckBox))
                {
                    CheckBox cb = (CheckBox)c;
                    cb.Checked = true;
                }
            }
            this.stationSearchTextBox.Enabled = true;

        }

        private void Collection_OnAlertMessage(object sender, AlertMessageEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void Collection_OnAlertIssued(object sender, AlertIssuedEventArgs e)
        {
            //MessageBox.Show("A " + e.Alert.Event + " has been issued.");
        }

        private void ClockTimer_Tick(object sender, EventArgs e)
        {
            ts = ts.Subtract(TimeSpan.FromSeconds(1));
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(() =>
                {
                    this.startPollingBtn.Text = ts.ToString(@"hh\:mm\:ss");
                }));
            }
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(() => 
                {
                    ASOSStation stn = (ASOSStation)this.stationListComboBox.SelectedItem;
                    this.GetObs(stn.Identifier);
                }));
            }
        }

        private void GetObs(string station, bool showOnChart=true)
        {
            JToken o = FeatureCollection.FetchData(String.Format("https://api.weather.gov/stations/{0}/observations/current", station), FeatureCollection.GetFeature);
            obs = WeatherObservation.FromJToken(o);
            this.temperatureTextBox.Text = Math.Round(obs.Temperature.Value, 0).ToString();
            this.windTextBox.Text = obs.Wind.Direction == null ? null : obs.Wind.Direction.Cardinal + " " + Math.Round(obs.Wind.Speed, 0);
            this.rhTextBox.Text = Math.Round(obs.RH.Value, 0).ToString() + "%";
            if (obs.HeatIndex != null)
            {
                this.heatIndexTextBox.Text = Math.Round((decimal)obs.HeatIndex.Value).ToString();
            }

            this.dewPointTextBox.Text = Math.Round(obs.DewPoint.Value, 2).ToString();
            if (obs.WindChill != null)
            {
                this.wcTextBox.Text = Math.Round(obs.WindChill.Value).ToString();
            }

            if (showOnChart)
            {
                this.chart1.Visible = true;
                this.chart1.Series["Temperature"].Points.AddXY(obs.Timestamp, (double)obs.Temperature);
                this.chart1.Series["Dew Point"].Points.AddXY(obs.Timestamp, (double)obs.DewPoint.Value);
                //this.chart1.Series["Pressure"].Points.AddXY(obs.Timestamp, obs.Pressure.Value);
            }
        }

        private void GetObs()
        {
            List<WeatherObservation> obs = WeatherObservation.ReadMetarFromCSV();

            foreach (WeatherObservation ws in obs)
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new Action(() => 
                    {
                        this.temperatureTextBox.Text = ws.Temperature.Value.ToString();
                        this.windTextBox.Text = ws.Wind.Direction.Cardinal + " " + ws.Wind.Speed;
                        this.rhTextBox.Text = ws.RH.Value.ToString() + "%";
                        if (ws.HeatIndex != null)
                        {
                            this.heatIndexTextBox.Text = Math.Round((decimal)ws.HeatIndex.Value).ToString();
                        }

                        this.dewPointTextBox.Text = Math.Round(ws.DewPoint.Value.ToFahrenheit().Value).ToString();
                        if (ws.WindChill != null)
                        {
                            this.wcTextBox.Text = Math.Round(ws.WindChill.Value).ToString();
                        }
                    }));
                }

            }
        }

        private void ToggleChartSeries(CheckBox cb, string series)
        {
            if (cb.Checked)
            {
                this.chart1.Series[series].Enabled = true;
            }
            else
            {
                this.chart1.Series[series].Enabled = false;
            }
        }

        private void tempCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            ToggleChartSeries(this.tempCheckbox, "Temperature");
        }

        private void dewPointCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            ToggleChartSeries(this.dewPointCheckbox, "Dew Point");
        }

        private void pressureCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ToggleChartSeries(this.pressureCheckBox, "Pressure");
        }

        private void chart1_MouseMove(object sender, MouseEventArgs e)
        {
            var pos = e.Location;
            if (previousPosition.HasValue && pos == previousPosition.Value)
                return;
            toolTip.RemoveAll();
            previousPosition = pos;
            var results = chart1.HitTest(pos.X, pos.Y, false,
                ChartElementType.DataPoint);

            foreach (var result in results)
            {
                if (result.ChartElementType == ChartElementType.DataPoint)
                {
                    var prop = result.Object as DataPoint;
                    if (prop != null)
                    {
                        var pointXPixel = result.ChartArea.AxisX.ValueToPixelPosition(prop.XValue);
                        var pointYPixel = result.ChartArea.AxisY.ValueToPixelPosition(prop.YValues[0]);

                        toolTip.Show("Date/Time=" + DateTime.FromOADate(prop.XValue) + ", Y=" + prop.YValues[0], this.chart1, pos.X, pos.Y - 15);
                    }
                }
            }
        }

        private void stationSearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            this.stationListComboBox.SelectedItem = ASOSStation.Search(ref this.stationList, this.stationSearchTextBox.Text);
        }

        private void getObsBtn_Click(object sender, EventArgs e)
        {
            ASOSStation stn = (ASOSStation)this.stationListComboBox.SelectedItem;
            this.GetObs(stn.Identifier, false);
        }

        private void startPollingBtn_Click(object sender, EventArgs e)
        {
            ASOSStation stn = (ASOSStation)this.stationListComboBox.SelectedItem;
            this.GetObs(stn.Identifier);
            timer.Enabled = true;
            timer.Start();
            clockTimer.Enabled = true;
            clockTimer.Start();
        }

        private async void filterButton_Click(object sender, EventArgs e)
        {
            await this.GetAlerts();
        }

        private void alertsDataGrid_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            Alert a = (Alert)alertsDataGrid.Rows[e.RowIndex].DataBoundItem;
            if (a != null)
            {
                alertsDataGrid.DefaultCellStyle.BackColor = a.PrimaryColor;
            }
        }
    }
}
