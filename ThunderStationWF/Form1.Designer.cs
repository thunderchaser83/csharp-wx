﻿namespace ThunderStationWF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.label1 = new System.Windows.Forms.Label();
            this.temperatureTextBox = new System.Windows.Forms.TextBox();
            this.dewPointTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.heatIndexTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rhTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.wcTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.windTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tempCheckbox = new System.Windows.Forms.CheckBox();
            this.dewPointCheckbox = new System.Windows.Forms.CheckBox();
            this.pressureCheckBox = new System.Windows.Forms.CheckBox();
            this.stationListComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.stationSearchTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.getObsBtn = new System.Windows.Forms.Button();
            this.startPollingBtn = new System.Windows.Forms.Button();
            this.alertsDataGrid = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.senderTextBox = new System.Windows.Forms.TextBox();
            this.expiresTextBox = new System.Windows.Forms.TextBox();
            this.headlineTextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.filterButton = new System.Windows.Forms.Button();
            this.SelectedAlertsCheckListBox = new System.Windows.Forms.CheckedListBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.warningLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.featureListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.featureCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.featureListBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alertsDataGrid)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.featureListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.featureCollectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.featureListBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Temperature";
            // 
            // temperatureTextBox
            // 
            this.temperatureTextBox.Location = new System.Drawing.Point(124, 16);
            this.temperatureTextBox.Name = "temperatureTextBox";
            this.temperatureTextBox.Size = new System.Drawing.Size(120, 20);
            this.temperatureTextBox.TabIndex = 1;
            // 
            // dewPointTextBox
            // 
            this.dewPointTextBox.Location = new System.Drawing.Point(124, 42);
            this.dewPointTextBox.Name = "dewPointTextBox";
            this.dewPointTextBox.Size = new System.Drawing.Size(120, 20);
            this.dewPointTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Dew Point";
            // 
            // heatIndexTextBox
            // 
            this.heatIndexTextBox.Location = new System.Drawing.Point(124, 68);
            this.heatIndexTextBox.Name = "heatIndexTextBox";
            this.heatIndexTextBox.Size = new System.Drawing.Size(120, 20);
            this.heatIndexTextBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Heat Index";
            // 
            // rhTextBox
            // 
            this.rhTextBox.Location = new System.Drawing.Point(124, 94);
            this.rhTextBox.Name = "rhTextBox";
            this.rhTextBox.Size = new System.Drawing.Size(120, 20);
            this.rhTextBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Relative Humidity";
            // 
            // wcTextBox
            // 
            this.wcTextBox.Location = new System.Drawing.Point(124, 120);
            this.wcTextBox.Name = "wcTextBox";
            this.wcTextBox.Size = new System.Drawing.Size(120, 20);
            this.wcTextBox.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Wind Chill";
            // 
            // windTextBox
            // 
            this.windTextBox.Location = new System.Drawing.Point(124, 146);
            this.windTextBox.Name = "windTextBox";
            this.windTextBox.Size = new System.Drawing.Size(120, 20);
            this.windTextBox.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Wind";
            // 
            // chart1
            // 
            this.chart1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Bottom;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 426);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "Temperature";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Dew Point";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Pressure";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(1699, 200);
            this.chart1.TabIndex = 12;
            this.chart1.Text = "chart1";
            this.chart1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chart1_MouseMove);
            // 
            // tempCheckbox
            // 
            this.tempCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tempCheckbox.AutoSize = true;
            this.tempCheckbox.Location = new System.Drawing.Point(1259, 403);
            this.tempCheckbox.Name = "tempCheckbox";
            this.tempCheckbox.Size = new System.Drawing.Size(86, 17);
            this.tempCheckbox.TabIndex = 13;
            this.tempCheckbox.Text = "Temperature";
            this.tempCheckbox.UseVisualStyleBackColor = true;
            this.tempCheckbox.CheckedChanged += new System.EventHandler(this.tempCheckbox_CheckedChanged);
            // 
            // dewPointCheckbox
            // 
            this.dewPointCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dewPointCheckbox.AutoSize = true;
            this.dewPointCheckbox.Location = new System.Drawing.Point(1346, 403);
            this.dewPointCheckbox.Name = "dewPointCheckbox";
            this.dewPointCheckbox.Size = new System.Drawing.Size(75, 17);
            this.dewPointCheckbox.TabIndex = 14;
            this.dewPointCheckbox.Text = "Dew Point";
            this.dewPointCheckbox.UseVisualStyleBackColor = true;
            this.dewPointCheckbox.CheckedChanged += new System.EventHandler(this.dewPointCheckbox_CheckedChanged);
            // 
            // pressureCheckBox
            // 
            this.pressureCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pressureCheckBox.AutoSize = true;
            this.pressureCheckBox.Location = new System.Drawing.Point(1433, 403);
            this.pressureCheckBox.Name = "pressureCheckBox";
            this.pressureCheckBox.Size = new System.Drawing.Size(67, 17);
            this.pressureCheckBox.TabIndex = 15;
            this.pressureCheckBox.Text = "Pressure";
            this.pressureCheckBox.UseVisualStyleBackColor = true;
            this.pressureCheckBox.CheckedChanged += new System.EventHandler(this.pressureCheckBox_CheckedChanged);
            // 
            // stationListComboBox
            // 
            this.stationListComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.stationListComboBox.FormattingEnabled = true;
            this.stationListComboBox.Location = new System.Drawing.Point(1400, 41);
            this.stationListComboBox.Name = "stationListComboBox";
            this.stationListComboBox.Size = new System.Drawing.Size(287, 21);
            this.stationListComboBox.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1354, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Station";
            // 
            // stationSearchTextBox
            // 
            this.stationSearchTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stationSearchTextBox.Location = new System.Drawing.Point(1400, 14);
            this.stationSearchTextBox.Name = "stationSearchTextBox";
            this.stationSearchTextBox.Size = new System.Drawing.Size(285, 20);
            this.stationSearchTextBox.TabIndex = 18;
            this.stationSearchTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.stationSearchTextBox_KeyUp);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1354, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Search";
            // 
            // getObsBtn
            // 
            this.getObsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.getObsBtn.Location = new System.Drawing.Point(1566, 71);
            this.getObsBtn.Name = "getObsBtn";
            this.getObsBtn.Size = new System.Drawing.Size(121, 23);
            this.getObsBtn.TabIndex = 20;
            this.getObsBtn.Text = "Get Observations";
            this.getObsBtn.UseVisualStyleBackColor = true;
            this.getObsBtn.Click += new System.EventHandler(this.getObsBtn_Click);
            // 
            // startPollingBtn
            // 
            this.startPollingBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.startPollingBtn.Location = new System.Drawing.Point(1566, 104);
            this.startPollingBtn.Name = "startPollingBtn";
            this.startPollingBtn.Size = new System.Drawing.Size(121, 23);
            this.startPollingBtn.TabIndex = 21;
            this.startPollingBtn.Text = "Start Polling";
            this.startPollingBtn.UseVisualStyleBackColor = true;
            this.startPollingBtn.Click += new System.EventHandler(this.startPollingBtn_Click);
            // 
            // alertsDataGrid
            // 
            this.alertsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.alertsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.alertsDataGrid.Location = new System.Drawing.Point(0, 0);
            this.alertsDataGrid.Name = "alertsDataGrid";
            this.alertsDataGrid.Size = new System.Drawing.Size(1699, 258);
            this.alertsDataGrid.TabIndex = 22;
            this.alertsDataGrid.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.alertsDataGrid_RowPrePaint);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 39);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1713, 655);
            this.tabControl1.TabIndex = 23;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.pressureCheckBox);
            this.tabPage1.Controls.Add(this.dewPointCheckbox);
            this.tabPage1.Controls.Add(this.startPollingBtn);
            this.tabPage1.Controls.Add(this.tempCheckbox);
            this.tabPage1.Controls.Add(this.temperatureTextBox);
            this.tabPage1.Controls.Add(this.getObsBtn);
            this.tabPage1.Controls.Add(this.chart1);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.stationSearchTextBox);
            this.tabPage1.Controls.Add(this.dewPointTextBox);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.stationListComboBox);
            this.tabPage1.Controls.Add(this.heatIndexTextBox);
            this.tabPage1.Controls.Add(this.rhTextBox);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.wcTextBox);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.windTextBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1705, 629);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Observations";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1705, 629);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Watches/Warnings/Advisories";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.alertsDataGrid);
            this.splitContainer1.Size = new System.Drawing.Size(1699, 623);
            this.splitContainer1.SplitterDistance = 361;
            this.splitContainer1.TabIndex = 25;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1699, 361);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Alert";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1133F));
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.senderTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.expiresTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.headlineTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.descriptionTextBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 2, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1693, 342);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Expires: ";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Sender: ";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Headline: ";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Description";
            // 
            // senderTextBox
            // 
            this.senderTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.senderTextBox.Location = new System.Drawing.Point(69, 3);
            this.senderTextBox.Name = "senderTextBox";
            this.senderTextBox.Size = new System.Drawing.Size(488, 20);
            this.senderTextBox.TabIndex = 7;
            // 
            // expiresTextBox
            // 
            this.expiresTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.expiresTextBox.Location = new System.Drawing.Point(69, 29);
            this.expiresTextBox.Name = "expiresTextBox";
            this.expiresTextBox.Size = new System.Drawing.Size(488, 20);
            this.expiresTextBox.TabIndex = 8;
            // 
            // headlineTextBox
            // 
            this.headlineTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.headlineTextBox.Location = new System.Drawing.Point(69, 55);
            this.headlineTextBox.Name = "headlineTextBox";
            this.headlineTextBox.Size = new System.Drawing.Size(488, 20);
            this.headlineTextBox.TabIndex = 9;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.descriptionTextBox.Location = new System.Drawing.Point(69, 94);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(488, 245);
            this.descriptionTextBox.TabIndex = 10;
            this.descriptionTextBox.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.filterButton);
            this.panel1.Controls.Add(this.SelectedAlertsCheckListBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(563, 94);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1127, 245);
            this.panel1.TabIndex = 12;
            // 
            // filterButton
            // 
            this.filterButton.Location = new System.Drawing.Point(250, 4);
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size(75, 23);
            this.filterButton.TabIndex = 12;
            this.filterButton.Text = "Filter";
            this.filterButton.UseVisualStyleBackColor = true;
            this.filterButton.Click += new System.EventHandler(this.filterButton_Click);
            // 
            // SelectedAlertsCheckListBox
            // 
            this.SelectedAlertsCheckListBox.FormattingEnabled = true;
            this.SelectedAlertsCheckListBox.Location = new System.Drawing.Point(3, 3);
            this.SelectedAlertsCheckListBox.Name = "SelectedAlertsCheckListBox";
            this.SelectedAlertsCheckListBox.Size = new System.Drawing.Size(240, 244);
            this.SelectedAlertsCheckListBox.TabIndex = 11;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 697);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1728, 22);
            this.statusStrip1.TabIndex = 23;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // statusStrip2
            // 
            this.statusStrip2.AutoSize = false;
            this.statusStrip2.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.warningLabel});
            this.statusStrip2.Location = new System.Drawing.Point(0, 0);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(1728, 36);
            this.statusStrip2.TabIndex = 24;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // warningLabel
            // 
            this.warningLabel.AutoSize = false;
            this.warningLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.warningLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.warningLabel.Name = "warningLabel";
            this.warningLabel.Size = new System.Drawing.Size(1713, 31);
            this.warningLabel.Spring = true;
            // 
            // featureListBindingSource
            // 
            this.featureListBindingSource.DataMember = "FeatureList";
            this.featureListBindingSource.DataSource = this.featureCollectionBindingSource;
            // 
            // featureCollectionBindingSource
            // 
            this.featureCollectionBindingSource.DataSource = typeof(GeoJsonWeather.FeatureCollection);
            // 
            // featureListBindingSource1
            // 
            this.featureListBindingSource1.DataMember = "FeatureList";
            this.featureListBindingSource1.DataSource = this.featureCollectionBindingSource;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1728, 719);
            this.Controls.Add(this.statusStrip2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alertsDataGrid)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.featureListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.featureCollectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.featureListBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox temperatureTextBox;
        private System.Windows.Forms.TextBox dewPointTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox heatIndexTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox rhTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox wcTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox windTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.CheckBox tempCheckbox;
        private System.Windows.Forms.CheckBox dewPointCheckbox;
        private System.Windows.Forms.CheckBox pressureCheckBox;
        private System.Windows.Forms.ComboBox stationListComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox stationSearchTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button getObsBtn;
        private System.Windows.Forms.Button startPollingBtn;
        private System.Windows.Forms.DataGridView alertsDataGrid;
        private System.Windows.Forms.BindingSource featureCollectionBindingSource;
        private System.Windows.Forms.BindingSource featureListBindingSource;
        private System.Windows.Forms.BindingSource featureListBindingSource1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox senderTextBox;
        private System.Windows.Forms.TextBox expiresTextBox;
        private System.Windows.Forms.TextBox headlineTextBox;
        private System.Windows.Forms.RichTextBox descriptionTextBox;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel warningLabel;
        private System.Windows.Forms.CheckedListBox SelectedAlertsCheckListBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button filterButton;
    }
}

