﻿using GeoJsonWeather;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThunderStationWF
{
    public partial class WarningForm : Form
    {
        public IAlert FormAlert { get; private set; }

        public WarningForm()
        {
            InitializeComponent();

            this.Size = new Size(Screen.PrimaryScreen.Bounds.Width, 50);
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(0, 0);
        }

        public void ChangeAlert(IAlert al)
        {
            this.FormAlert = al;
            this.BackColor = FormAlert.PrimaryColor;
            this.alertLabel.ForeColor = FormAlert.SecondaryColor;
            this.alertLabel.Text = String.Format("A new {0} has been issued for, {1} until {2}", FormAlert.Event, FormAlert.AreaDescription, FormAlert.Expires);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
